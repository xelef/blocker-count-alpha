// ==UserScript==
// @id             iitc-plugin-blocker-count@xelef
// @name           IITC plugin: blocker count
// @version        1.0.0.1
// @namespace      https://github.com/jonatkins/ingress-intel-total-conversion
// @updateURL      none
// @downloadURL    none
// @description    XPERIMENTAL: Counts blockers. Requires cross-links plugin.
// @include        https://www.ingress.com/intel*
// @include        http://www.ingress.com/intel*
// @match          https://www.ingress.com/intel*
// @match          http://www.ingress.com/intel*
// @include        https://www.ingress.com/mission/*
// @include        http://www.ingress.com/mission/*
// @match          https://www.ingress.com/mission/*
// @match          http://www.ingress.com/mission/*
// @grant          none
// ==/UserScript==


function wrapper(plugin_info) {
// ensure plugin framework is there, even if iitc is not yet loaded
if(typeof window.plugin !== 'function') window.plugin = function() {};

//PLUGIN AUTHORS: writing a plugin outside of the IITC build environment? if so, delete these lines!!
//(leaving them in place might break the 'About IITC' page or break update checks)
// plugin_info.buildName = 'local';
// plugin_info.dateTimeVersion = '20160921.35613';
// plugin_info.pluginId = 'cross_link';
//END PLUGIN AUTHORS NOTE



// PLUGIN START ////////////////////////////////////////////////////////

var BlockerCount = (function () {
  function BlockerCount() {
    this.defaultCounts = [ 0, 0, 0 ];

    this.counts = this.emptyCounts();
    this.totalBlockers = 0;
    this.text = 'Awaiting update'

    this.animationInfo = undefined;
    this.updateTimer = undefined;
    this.control = undefined;
  }

  BlockerCount.prototype.emptyAnimationInfo = function() {
    return {
      weight: 100,
      linkRefs: [],
      polys: []
    };
  };

  BlockerCount.prototype.emptyCounts = function() {
    return [ 0, 0, 0 ];
  };

  // BlockerCount.prototype.dup = function(obj) {
  //   var duplicate = {};

  //   Object.keys(obj).forEach(function (key) {
  //     if (obj.hasOwnProperty(key)) {
  //       duplicate[key] = obj[key];
  //     }
  //   });

  //   return duplicate;
  // };

  BlockerCount.prototype.setup = function() {
    if (window.plugin.crossLinks === undefined) {
      alert("blocker-count requires cross-links");
      return;
    }

    // this plugin also needs to create the draw-tools hook, in case it is initialised before draw-tools
    window.pluginCreateHook('pluginDrawTools');

    // events
    window.addHook('pluginDrawTools',
      function(e) {
        // Any event, trigger an update.
        this.markForUpdate();
      }.bind(this)
    );

    window.addHook('linkAdded', this.onLinkAdded.bind(this));
    window.addHook('mapDataRefreshEnd', this.onMapDataRefreshEnd.bind(this));

    // $('#toolbox').append(' <a onclick="this.displayDialog()" title="Display count of blockers for current plan">Blocker Count</a>');

    var blockerCountControl = this.createControl();
    this.control = new blockerCountControl();
    map.addControl(this.control);

    setInterval(this.markForUpdate.bind(this), 5000);
  };

  BlockerCount.prototype.createControl = function() {
    var control_init = function (map) {
      this.container = L.DomUtil.create('div', 'leaflet-bar leaflet-control leaflet-control-custom');

      this.container.title = this.text;

      this.container.style.backgroundColor = 'white';
      this.container.style.width = '26px';
      this.container.style.height = '26px';

      this.container.onclick = this.highlightBlockers.bind(this);

      return this.container;
    }.bind(this);

    var control_update = function () {
      this.container.title = this.text;

      if (this.totalBlockers > 0) {
        this.container.style.backgroundColor = 'red';
      }
      else {
        this.container.style.backgroundColor = 'white';
      }
    }.bind(this);

    return L.Control.extend({
      options: {
        position: 'topleft' 
      },

      onAdd: control_init,
      update: control_update
    });

    // map.addControl(this.control);
  };

  BlockerCount.prototype.update = function() {
    var animationInfo = this.emptyAnimationInfo();
    var counts = this.emptyCounts();
    var totalBlockers = 0;

    Object.keys(plugin.crossLinks.linkLayer._layers).forEach(function (key) { 
      var blocker = plugin.crossLinks.linkLayer._layers[key];

      if (blocker) {
        var opts = blocker.options;

        if (opts) {
          var guid = opts.guid;
          var link = links[guid];

          if (link && link.options && link.options.data) {
            var latlngs = link.getLatLngs();
            var team = link.options.team;

            if (team) {
              counts[team]++;
              totalBlockers++;

              animationInfo.linkRefs.push({
                team: team,
                latlngs: latlngs,
                guid: guid
              });
            }
          }
        }
      }
    });

    // Once all that's done, update the actual world.
    this.counts = counts;
    this.totalBlockers = totalBlockers;
    this.text = "RES " + counts[1] + " ENL " + counts[2];
    this.animationInfo = animationInfo;

    if (this.control) {
      this.control.update();
    }

    console.log("blockerCount: " + this.text);
  }

  BlockerCount.prototype.markForUpdate = function() {
    if (this.updateTimer) {
      // console.log("blockerCount: cancelling update");
      clearTimeout(this.updateTimer);
    }

    // console.log("blockerCount: scheduling update");
    this.updateTimer = setTimeout(this.update.bind(this), 100);
  }

  BlockerCount.prototype.onLinkAdded = function(data) {
    this.markForUpdate();
  };

  BlockerCount.prototype.onMapDataRefreshEnd = function() {
    this.markForUpdate();
  }

  BlockerCount.prototype.animateBlockers = function(animationInfo) {
    if (animationInfo.polys.length > 0) {
      // console.log("aB " + animationInfo.weight + " removing old polys");
      animationInfo.polys.forEach(function (poly) { map.removeLayer(poly); });
      animationInfo.polys = [];
    }

    if ((animationInfo.weight > 0) &&
        (animationInfo.linkRefs.length > 0)) {
      // console.log("aB " + animationInfo.weight + " adding polys (" + animationInfo.linkRefs.length + ")");

      animationInfo.linkRefs.forEach(function (linkRef) {
        var poly = L.geodesicPolyline(linkRef.latlngs, {
          color: 'red',
          opacity: 0.3,
          weight: animationInfo.weight,
          clickable: false,
          dashArray: [8,8],
          guid: linkRef.guid
        });

        animationInfo.polys.push(poly);
        map.addLayer(poly);
      });

      // console.log("aB " + animationInfo.weight + " scheduling next call");

      animationInfo.weight -= 10;

      setTimeout(function () { this.animateBlockers(animationInfo); }.bind(this), 20);
    }
    else {
      // console.log("aB " + animationInfo.weight + " done");
      this.animationInfo = undefined;
    }
  }

  BlockerCount.prototype.highlightBlockers = function() {
    if (this.animationInfo != undefined) {
      var animationInfo = this.animationInfo;
      this.animationInfo = undefined;

      this.animateBlockers(animationInfo);
    }
  }

  // // The final function that displays the scoreboard by calling the portalTable function
  // this.displayDialog = function () {
  //   dialog({
  //     html: '<div id="blockerCount-div">' + this.html + '</div>',
  //     dialogClass: 'ui-dialog-blockerCount',
  //     title: 'Blockers',
  //     id: 'Blockers'
  //     // ,
  //     // width: 100,
  //     // height: 50
  //   });

  //   this.updateable = $('#blockerCount-div')[0];
  //   this.markForUpdate();
  // }

  return BlockerCount;
}());

window.plugin.BlockerCount = new BlockerCount();

var setup = function() {
  window.plugin.BlockerCount.setup();
}

// PLUGIN END //////////////////////////////////////////////////////////


setup.info = plugin_info; //add the script info data to the function as a property
if(!window.bootPlugins) window.bootPlugins = [];
window.bootPlugins.push(setup);
// if IITC has already booted, immediately run the 'setup' function
if(window.iitcLoaded && typeof setup === 'function') setup();
} // wrapper end
// inject code into site context
var script = document.createElement('script');
var info = {};
if (typeof GM_info !== 'undefined' && GM_info && GM_info.script) info.script = { version: GM_info.script.version, name: GM_info.script.name, description: GM_info.script.description };
script.appendChild(document.createTextNode('('+ wrapper +')('+JSON.stringify(info)+');'));
(document.body || document.head || document.documentElement).appendChild(script);


