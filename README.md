BlockerCount
============

This plugin provides a simple control that indicates whether or not any links are blocking your plan. It relies on IITC and on the `cross-links` plugin.

Installation
------------

You can install the plugin [direct from Bitbucket](https://bitbucket.org/xelef/blocker-count-alpha/raw/master/blocker_count.user.js) or, of course, by cloning [the repo](https://bitbucket.org/xelef/blocker-count-alpha) and installing from your local system.

Use
---

At present, the blocker indication is a small blank square in the toolbar on the left side. If the square is _white_, all is well: there are no unresolved blockers. If it is _red_, there are unresolved blockers.

Mouse over the control to see a count of RES and ENL blockers.

Click the control to highlight blockers (this is particularly useful if the blockers are too short, and the map too cluttered, to easily see the blockers).

*NOTE WELL*: only blockers that `cross-links` can see can be counted. This plugin is most useful with the map at the "all links" zoom level, and your entire link plan on the screen. That may or may not be easy to actually arrange...
